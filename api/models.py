import logging

from django.db import models

# Create your models here.
logger = logging.getLogger(__name__)


class ApplicationUser(models.Model):
    logger.info("In user api")
    name = models.CharField(max_length=30)
    age = models.IntegerField()
    domain = models.CharField(max_length=30)
