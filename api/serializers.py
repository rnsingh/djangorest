"""
File Name : serializers.py
Author : Ravi
Date : 11/3/2017
Description :

"""
from rest_framework import serializers

from api.models import ApplicationUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationUser
        fields = ('name', 'age', 'domain')