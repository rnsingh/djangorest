from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, permissions

from api.models import ApplicationUser
from api.serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    """

    """
    queryset = ApplicationUser.objects.all()
    serializer_class = UserSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
